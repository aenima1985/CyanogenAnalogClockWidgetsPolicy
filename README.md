# Cyanogen Analog Clock Widgets Policy

Tommaso Matteuzzi built the Cyanogen Analog Clock Widgets app as a Free app. This APPLICATION is provided by Tommaso Matteuzzi at no cost and is intended for use as is.

This page is used to inform website visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my App. NONE your data is being collected or readed in any way. The App need a specific permission, READ_PHONE_STATE, to work and so this Privacy Policy is mandatory.

If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact me.
